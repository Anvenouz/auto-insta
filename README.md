# Auto Insta

Automatique leverage/save from insta api.

Exemple .env
```
# Cycle in min
TICKING=3
# config leverage
RATIO_TARGET=0.50
RATIO_MAX=0.51
RATIO_MIN=0.49
COL_TOKEN=dai
DEBT_TOKEN=eth
# Config stop win/lost in DAI for the bet price.
# In this config : ETH
STOP_WIN=4450
STOP_LOST=4200
# Config App test mode
DRYRUN=false
# Info Accounts
ETH_NODE_URL=https://polygon-mainnet.infura.io/v3/xxxxxxxxxxxxxxxxxxxxx
PRIVATE_KEY=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
ACCOUNT_ADDR=0x243AbC345ef9bF6E822fb1b9FE937fd9C0921848
ETH_ADDR=0x14555a1f32A9E936f367DB1ce63f6eA7a208d9E0
CHAINE_ID_MATIC=137
DSA_ID_ACCOUNT=48173
```

Launch app : 
- npm install
- npm link (one time)
- auto (name of folder project)

Example in bash : 
`auto >> logs.txt 2>&1 &` # use screen for detach your job.

