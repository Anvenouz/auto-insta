#!/usr/bin/env node

require('dotenv').config();
//Config levier :
let RATIO_TARGET = parseFloat(process.env.RATIO_TARGET);
let RATIO_MAX    = parseFloat(process.env.RATIO_MAX);
let RATIO_MIN    = parseFloat(process.env.RATIO_MIN);
const COL_TOKEN  = process.env.COL_TOKEN;
const DEBT_TOKEN = process.env.DEBT_TOKEN;
const DRYRUN     = (process.env.DRYRUN === 'true');
const TICKING    =  parseInt(process.env.TICKING) > 1 ? parseInt(process.env.TICKING) * 1000 * 60 : 3 * 1000 * 60;
const MAX_TRANSACTION_PENDING = 2;
const ENV_SLIPPAGE = parseFloat(process.env.SLIPPAGE);
const SLIPPAGE = (ENV_SLIPPAGE>= 0.1 && ENV_SLIPPAGE <= 3) ? ENV_SLIPPAGE : 1;

//STOP PRICE
const STOP_WIN = parseFloat(process.env.STOP_WIN);
const STOP_LOST= parseFloat(process.env.STOP_LOST);


//Config accounts :
const ETH_NODE_URL    = process.env.ETH_NODE_URL;
const PRIVATE_KEY     = process.env.PRIVATE_KEY;
const ACCOUNT_ADDR    = process.env.ACCOUNT_ADDR;
const ETH_ADDR        = process.env.ETH_ADDR;
const CHAINE_ID_MATIC = parseInt(process.env.CHAINE_ID_MATIC);
const DSA_ID_ACCOUNT  = parseInt(process.env.DSA_ID_ACCOUNT); //Correspond à l'adresse de compte via numéro genre le numéro du "vault"

const PARASWAP = true;

//Increase gase price
const INCREASE_GAS_PRICE = 1.1;
const INCREASE_GAS = 1.1;

//Require :
const Web3 = require('web3');
const DSA = require('dsa-connect');
const axios = require('axios'); //For 1INCH API
const BigNumber = require('bignumber.js');
BigNumber.config({ EXPONENTIAL_AT: [-10, 30] });
let nonce = 0;
let forceNonce = 0;

function createTokenUtils(tokens) {
  const getTokenByAddress = address =>
    tokens.find(token => token.address === address);
  const getTokenByKey = key => tokens.find(token => token.key === key);
  const tokenKeys = tokens.map(token => token.key);
  const rootTokens = tokens.map(token => token.root);
  const getTokenByRoot = root => tokens.find(token => token.root === root);
  const getDebtTokenByDebtRoot = root => tokens.find(token => token.dRoot === root);

  return {
    allTokens: tokens,
    tokenKeys,
    getTokenByAddress,
    getTokenByKey,
    rootTokens,
    getTokenByRoot,
    getDebtTokenByDebtRoot,
  };
}

function getKeyByValue(object, value) {
  return Object.keys(object).find(key => object[key] === value);
}

const DAI_TOKEN   = '0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063';
const MATIC_TOKEN = '0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE';
const ETH_TOKEN   = '0x7ceb23fd6bc0add59e62ac25578270cff1b9f619';
const BTC_TOKEN   = '0x1bfd67037b42cf73acf2047067bd4f2c47d9bfd6';
const WMATIC_TOKEN = '0x0d500B1d8E8eF31E21C99d1Db9A6444d3ADf1270';

const token = createTokenUtils([
    { key: "aeth", "type": "atoken", "symbol": "AETH", "name": "Aave ETH", "address": "0x28424507fefb6f7f8E9D3860F56504E4e5f5f390", "decimals": 18, "factor": 0.8, "root": "eth", "rootAddress": ETH_TOKEN },
    { key: "adai", "type": "atoken", "symbol": "ADAI", "name": "Aave DAI", "address": "0x27F8D03b3a2196956ED754baDc28D73be8830A6e", "decimals": 18, "factor": 0.75, "root": "dai", "rootAddress": DAI_TOKEN },
    { key: "ausdc", "type": "atoken", "symbol": "AUSDC", "name": "Aave USDC", "address": "0x1a13F4Ca1d028320A707D99520AbFefca3998b7F", "decimals": 6, "factor": 0.8, "root": "usdc" },
    { key: "ausdt", "type": "atoken", "symbol": "AUSDT", "name": "Aave USDT", "address": "0x60D55F02A771d515e077c9C2403a1ef324885CeC", "decimals": 6, "factor": 0, "root": "usdt" },
    { key: "awbtc", "type": "atoken", "symbol": "AWBTC", "name": "Aave WBTC", "address": "0x5c2ed810328349100A66B82b78a1791B101C9D61", "decimals": 8, "factor": 0.7, "root": "wbtc", "rootAddress": BTC_TOKEN},
    { key: "aaave", "type": "atoken", "symbol": "AAAVE", "name": "Aave AAVE", "address": "0x1d2a0E5EC8E5bBDCA5CB219e649B565d8e5c3360", "decimals": 18, "factor": 0.5, "root": "aave" },
    { key: "awmatic", "type": "atoken", "symbol": "AWMATIC", "name": "Aave WMATIC", "address": "0x8dF3aad3a84da6b69A4DA8aeC3eA40d9091B2Ac4", "decimals": 18, "factor": 0.65, "root": "matic", "rootAddress": MATIC_TOKEN },
    {key: "variableDebtmDAI", "address" : "0x75c4d1fb84429023170086f06e682dcbbf537b7d",  "tokenFrom": DAI_TOKEN , "dRoot":"dai"},
    {key: "variableDebtmWETH", "address" : "0xede17e9d79fc6f9ff9250d9eefbdb88cc18038b5", "tokenFrom": ETH_TOKEN, "dRoot":"eth"},
    {key: "variableDebtmWBTC", "address" : "0xf664f50631a6f0d72ecdaa0e49b0c019fa72a8dc", "tokenFrom": BTC_TOKEN, "dRoot":"btc"},
    {key: "variableDebtmWMATIC", "address" : "0x59e8e9100cbfcbcbadf86b9279fa61526bbb8765", "tokenFrom": MATIC_TOKEN, "dRoot":"matic"}
]);

//Connect
const web3 = new Web3(new Web3.providers.HttpProvider(ETH_NODE_URL));
const dsa = new DSA({
    web3: web3,
    mode: 'node',
    privateKey: PRIVATE_KEY
}, CHAINE_ID_MATIC);

async function cleanUselessDebt(clean, dryrun = false) {
    let spells = dsa.Spell();
    let account = await dsa.setInstance(DSA_ID_ACCOUNT);
    for(const debtName in clean) {
        let tokenAddr = token.getTokenByKey(debtName).tokenFrom;
        console.log(`${debtName} : ` + clean[debtName].inDai);
        // console.log('inTokenToWai : ' + clean[debtName].inTokenToWei);
        spells = await cleanDebt(spells,
            tokenAddr, //From
            clean[debtName].inTokenToWei,  //Amount
        );
    }
    return await createCast(spells, dryrun);
}

/**
 *
 * @param fromToken Adresse du token à vendre
 * @param toToken   Adresse du token à acheter
 * @param amount    Montant en decimales à acheter /!\
 * @param slippage  Ecart d'achat max en pourcentage
 * @param borrowWMatic Emprunt chez matic pour leverage
 * @param dryrun seulement effectuer le gase price/quatité
 * @returns {Promise<void>}
 */
async function castLeverage(fromToken, toToken, amount, slippage, borrowWMatic, dryrun) {
    let spells = dsa.Spell();
    let account = await dsa.setInstance(DSA_ID_ACCOUNT);
    spells = await leverage(spells,
        toToken,  //To
        fromToken, //From
        amount,   //Amount
        slippage,  //slippage in %
        account.address, //address from the account InstaDapp
        borrowWMatic, //Borrow some Matic from AVEE
    );
    return await createCast(spells, dryrun);
}


/**
 *
 * @param fromToken Adresse du token à vendre (ici le collatérale)
 * @param toToken   Adresse du token à acheter (ici la debt)
 * @param amount    Montant en decimales à VENDRE (en collatérale) /!\
 * @param slippage  Ecart d'achat max en pourcentage
 * @param borrowWMatic Emprunt chez matic pour leverage
 * @param dryrun seulement effectuer le gase price/quatité
 * @returns {Promise<void>}
 */
async function castSave(fromToken, toToken, amount, slippage, borrowWMatic, dryrun) {
    let spells = dsa.Spell();
    let account = await dsa.setInstance(DSA_ID_ACCOUNT);

    spells = await save(spells,
        fromToken, //From
        toToken,  //To
        amount,   //Amount
        slippage,  //slippage in %
        account.address, //address from the account InstaDapp
        borrowWMatic, //Borrow some Matic from AVEE
    );
    return await createCast(spells, dryrun);
}

async function createCast(spells, dryrun = false) {
    let gasPrice = await web3.eth.getGasPrice();
    //Add increase gas price for faster transaction
    let gasPriceMore  = (gasPrice  * INCREASE_GAS_PRICE).toFixed(0);
    // EstimateCastGas Permet aussi de vérifier que la transaction fonctionnera !
    let gas = await spells.estimateCastGas();
    let estimateGas = (gas * INCREASE_GAS).toFixed(0);
    let optionsCasts = {
      gasPrice: gasPriceMore, // in gwei, used in node implementation.
      gas: estimateGas, // sending 1 Eth along the transaction.
      nonce: nonce
    };
    // console.log(optionsCasts);
    let costTransaction = (((gasPriceMore) * estimateGas ) / Math.pow(10, 18));
    console.log('--------------------');
    console.log('Nonce : ' + nonce);
    // console.log('Estimate Gas : ' + estimateGas);
    // console.log('Gas Price : ' + gasPriceMore);
    console.log('Tx Cost : ' + costTransaction + ' Matic');
    console.log('--------------------');
    if(!dryrun) {
        //Execute the transaction !
        return await spells.cast(optionsCasts);
    }
}

//1INCH API :
function url1inch(fromToken, toToken, amount, slippage, address) {
    // the URL of the tokens you may want to swap, change if the provider isn't matic
    let r = 'https://api.1inch.exchange/v4.0/137/swap?fromTokenAddress='+ fromToken +
        '&toTokenAddress='+ toToken +
        '&amount=' + amount +
        '&fromAddress=' + address +
        '&slippage='+slippage +
        '&disableEstimate=true';
    return r;
}

//1INCH API :
async function urlparaswap(fromToken, toToken, amount, slippage, address) {
    // the URL of the tokens you may want to swap, change if the provider isn't matic
    let r = 'https://apiv5.paraswap.io/prices?srcToken='+ fromToken +
        '&destToken='+ toToken +
        '&amount=' + amount +
        '&fromAddress=' + address +
        '&slippage='+ 1 +
        '&network='+ CHAINE_ID_MATIC + '';

    let temp = await axios.get(r);
    let priceRoute = temp.data;
    priceRoute = {
        "srcToken": fromToken,
        "destToken": toToken,
        "srcAmount": amount,
        "priceRoute" : priceRoute.priceRoute,
        "userAddress": address,
        "slippage":  slippage * 100, //for 1%
    };

    let post = await axios.post('https://apiv5.paraswap.io/transactions/'+CHAINE_ID_MATIC+'?ignoreChecks=true', JSON.stringify(priceRoute), {
        headers: {
            'Content-Type': 'application/json',
            'accept': 'application/json'
        }
    });
    post['priceRoute'] = priceRoute.priceRoute;

    //Compatibilité with 1inch !!
    let caller = {
      toToken: {address : toToken, decimals : post.priceRoute.destDecimals},
      fromToken: {address : fromToken, decimals: post.priceRoute.srcDecimals},
      fromTokenAmount: amount,
      toTokenAmount : post.priceRoute.bestRoute[0].swaps[0].swapExchanges[0].destAmount,
      tx : {data : post.data.data},
      priceRoute : post.priceRoute
    };
    return caller;
}



/**
 * Will call the api and return the data needed
 * @param {the url of what api call you want} url
 * @returns swap transaction
 */
async function apiCaller(url) {
    let temp = await axios.get(url);                //get the api call
    temp = temp.data;                               //we only want the data object from the api call
    delete temp.tx.gasPrice;                        //ethersjs will find the gasPrice needed
    delete temp.tx.gas;                             //ethersjs will find the gasLimit for users

    //we also need value in the form of hex
    let value = parseInt(temp.tx["value"]);			//get the value from the transaction
    value = '0x' + value.toString(16);				//add a leading 0x after converting from decimal to hexadecimal
    temp.tx["value"] = value;						//set the value of value in the transaction object. value referrs to how many of the native token

    //temp.tx["nonce"] = nonce;                     //ethersjs will find the nonce for the user
    //temp.tx.chainId = 137                         //this allows the transaction to NOT be replayed on other chains, ethersjs will find it for the user
    return temp;                                    //return the data
}

async function apiGetPrices() {
    let temp = await axios.get('https://min-api.cryptocompare.com/data/price?fsym=DAI&tsyms=ETH,MATIC,BTC,DAI');
    let prices = {};
    for(const key in temp.data) {
        prices[key.toLocaleLowerCase()] = 1 / temp.data[key];
    }
    return prices;
}


async function colSwap(spells, fromToken, toToken, amount, slippage, address) {
    let tpAmount = BigNumber(amount) / BigNumber(1.00501) ;
    let tpAmountFee = BigNumber(amount); //Ils prennent une taxe donc attention !
    let data1inch = await getData1Inch(fromToken, toToken, tpAmount, slippage, address);

    //Calculer cette unit amount pourquoi alors qu'ils ont les infos ? fin bon c'est dans la doc :
    //https://docs.instadapp.io/faq/connectors/calculate-unitamt
    //Pas facile à trouver !
    let unitAmt = caculateUnitAmt(data1inch.toTokenAmount, data1inch.fromTokenAmount , data1inch.toToken.decimals, data1inch.fromToken.decimals, slippage);
    const n = web3.utils.randomHex(32);
    spells.add({
      connector: "1INCH-V4-A",
      method: "sell",
      args: [
          toToken,   //ADDR BUY
          fromToken, //ADDR SELL
          BigNumber(data1inch.fromTokenAmount),   //AMOUNT SELL
          unitAmt,
          data1inch.tx.data,
          n
      ]
    });

    //Ceci corrige le low-level call failed ! effectivement le slippage n'était pas pris en compte !
    // let deposit = (data1inch.toTokenAmount - (data1inch.toTokenAmount * (slippage/100))).toFixed(0);
    //Optionnel avec le n
    spells.add({
      connector: 'aave_v2',
      method: 'deposit',
      args: [
          toToken,
          0,
          n,
          0
      ],
    });

    spells.add({
      connector: 'aave_v2',
      method: 'withdraw',
      args: [
          fromToken,
          BigNumber(tpAmountFee),
          0,
          0
      ],
    });

    spells.add({
        connector: 'INSTAPOOL-C',
        method: 'flashPayback',
        args: [
            fromToken,
            BigNumber(tpAmountFee),
            0,
            0
        ],
    });

    let cd = dsa.instapool_v2.encodeFlashCastData(spells);
    let flashloanWrappedSpells = dsa.Spell();

    // console.log([fromToken, BigNumber(tpAmount).toString(), "5", cd, "0x"]);
    // process.exit(1);

    flashloanWrappedSpells.add({
        connector: 'INSTAPOOL-C',
        method: 'flashBorrowAndCast',
        args: [fromToken, BigNumber(tpAmount), "5", cd, "0x"],
    });

    return flashloanWrappedSpells;
}

//https://docs.instadapp.io/faq/connectors/calculate-unitamt
function caculateUnitAmt(buyAmount, sellAmount, buyDecimal, sellDecimal, maxSlippage) {
  let unitAmt = BigNumber(buyAmount)
    .dividedBy(10 ** buyDecimal)
    .dividedBy(BigNumber(sellAmount).dividedBy(10 ** sellDecimal))
  unitAmt = unitAmt.multipliedBy((100 - maxSlippage) / 100)
  unitAmt = unitAmt.multipliedBy(1e18).toFixed(0)
  return unitAmt
}


async function leverage(spells, fromToken, toToken, amount, slippage, address, borrowWMatic = -1) {
    let tpAmount = BigNumber(amount);
    let tpAmountFee = BigNumber(tpAmount) * 1 * BigNumber(1.00501); //Ils prennent une taxe donc attention !
    let data1inch = await getData1Inch(fromToken, toToken, tpAmount, slippage, address);

    if(!(borrowWMatic > 0)) {
        spells.add({
          connector: 'aave_v2',
          method: 'borrow',
          args: [
              fromToken,
              BigNumber(tpAmount),
              2,
              0,
              0
          ],
        });
    }

    //Calculer cette unit amount pourquoi alors qu'ils ont les infos ? fin bon c'est dans la doc :
    //https://docs.instadapp.io/faq/connectors/calculate-unitamt
    //Pas facile à trouver !
    let unitAmt = caculateUnitAmt(data1inch.toTokenAmount, data1inch.fromTokenAmount , data1inch.toToken.decimals, data1inch.fromToken.decimals, slippage);
    const n = web3.utils.randomHex(32);

    let connector = '1INCH-V4-A';
    let method = 'sell';
    if(PARASWAP === true) {
        connector = 'PARASWAP-A';
        method = 'swap';
    }

    spells.add({
      connector: connector,
      method: method,
      args: [
          toToken,   //ADDR BUY
          fromToken, //ADDR SELL
          BigNumber(data1inch.fromTokenAmount),   //AMOUNT SELL
          unitAmt,
          data1inch.tx.data,
          n
      ]
    });

    //Ceci corrige le low-level call failed ! effectivement le slippage n'était pas pris en compte !
    // let deposit = (data1inch.toTokenAmount - (data1inch.toTokenAmount * (slippage/100))).toFixed(0);
    //Optionnel avec le n

    spells.add({
      connector: 'aave_v2',
      method: 'deposit',
      args: [
          toToken,
          0,
          n,
          0
      ],
    });

    if(borrowWMatic > 0) {
        spells.add({
          connector: 'aave_v2',
          method: 'borrow',
          args: [
              fromToken,
              BigNumber(tpAmountFee),
              2,
              0,
              0
          ],
        });

        spells.add({
            connector: 'INSTAPOOL-C',
            method: 'flashPayback',
            args: [
                fromToken,
                BigNumber(tpAmountFee),
                0,
                0
            ],
        });

        const cd = dsa.instapool_v2.encodeFlashCastData(spells);
        let flashloanWrappedSpells = dsa.Spell();

        flashloanWrappedSpells.add({
            connector: 'INSTAPOOL-C',
            method: 'flashBorrowAndCast',
            args: [fromToken, BigNumber(tpAmount), "1", cd, "0x"],
        });

        return flashloanWrappedSpells;
    }

    return spells;
}


async function save(spells, fromToken, toToken, amount, slippage, address, borrowWMatic = -1) {
    let tpAmount = BigNumber(amount);
    let tpAmountFee = BigNumber(tpAmount) * 1 * BigNumber(1.00501); //Ils prennent une taxe donc attention ?
    let data1inch = await getData1Inch(fromToken, toToken, tpAmount, slippage, address);

    if(!(borrowWMatic > 0)) {
        spells.add({
            connector: 'aave_v2',
            method: 'withdraw',
            args: [
                fromToken,
                BigNumber(tpAmount),
                0,
                0
            ],
        });
    }

    //Calculer cette unit amount pourquoi alors qu'ils ont les infos ? fin bon c'est dans la doc :
    //https://docs.instadapp.io/faq/connectors/calculate-unitamt
    //Pas facile à trouver !
    let unitAmt = caculateUnitAmt(data1inch.toTokenAmount, data1inch.fromTokenAmount , data1inch.toToken.decimals, data1inch.fromToken.decimals, slippage);
    const n = web3.utils.randomHex(32);

    let connector = '1INCH-V4-A';
    let method = 'sell';
    if(PARASWAP === true) {
        connector = 'PARASWAP-A';
        method = 'swap';
    }

    spells.add({
      connector: connector,
      method: method,
      args: [
          toToken,   //ADDR BUY
          fromToken, //ADDR SELL
          BigNumber(data1inch.fromTokenAmount),   //AMOUNT SELL
          unitAmt,
          data1inch.tx.data,
          n
      ]
    });

    spells.add({
      connector: 'aave_v2',
      method: 'payback',
      args: [
          toToken,
          BigNumber(data1inch.toTokenAmount), //Le max value n'a jamais marché chez moi !
          2,
          n,
          0
      ],
    });


    //Je ne pense pas que l'ordre des "methodes" soient importante mais ça marche bien donc je laisse le doublons
    //finalement ça en a ! xD
    if(borrowWMatic > 0) {
        spells.add({
          connector: 'aave_v2',
          method: 'withdraw',
          args: [
              fromToken,
              BigNumber(tpAmountFee),
              0,
              0
          ],
        });

        spells.add({
            connector: 'INSTAPOOL-C',
            method: 'flashPayback',
            args: [
                fromToken,
                BigNumber(tpAmountFee),
                0,
                0
            ],
        });

        const cd = dsa.instapool_v2.encodeFlashCastData(spells);
        let flashloanWrappedSpells = dsa.Spell();

        flashloanWrappedSpells.add({
            connector: 'INSTAPOOL-C',
            method: 'flashBorrowAndCast',
            args: [fromToken, BigNumber(tpAmount), "1", cd, "0x"],
        });

        return flashloanWrappedSpells;
    }

    return spells;
}

async function cleanDebt(spells, fromToken, amount) {
    let tpAmount = BigNumber(amount) / BigNumber(1.00501) ;
    let tpAmountFee = BigNumber(amount); //Ils prennent une taxe donc attention !
    //Ceci corrige le low-level call failed ! effectivement le slippage n'était pas pris en compte !
    // let deposit = (data1inch.toTokenAmount - (data1inch.toTokenAmount * (slippage/100))).toFixed(0);

    spells.add({
      connector: 'aave_v2',
      method: 'payback',
      args: [
          fromToken,
          BigNumber(tpAmount),
          2,
          0,
          0
      ],
    });

    spells.add({
      connector: 'aave_v2',
      method: 'withdraw',
      args: [
          fromToken,
          BigNumber(tpAmountFee),
          0,
          0
      ],
    });

    spells.add({
        connector: 'INSTAPOOL-C',
        method: 'flashPayback',
        args: [
            fromToken,
            BigNumber(tpAmountFee),
            0,
            0
        ],
    });

    let cd = dsa.instapool_v2.encodeFlashCastData(spells);
    let flashloanWrappedSpells = dsa.Spell();

    flashloanWrappedSpells.add({
        connector: 'INSTAPOOL-C',
        method: 'flashBorrowAndCast',
        args: [fromToken, BigNumber(tpAmount), "5", cd, "0x"],
    });

    return flashloanWrappedSpells;
}

async function debtSwap(spells, fromToken, toToken, amount, slippage, address) {
    let tpAmount = BigNumber(amount) / BigNumber(1.00501) ;
    let tpAmountFee = BigNumber(amount); //Ils prennent une taxe donc attention !
    let data1inch = await getData1Inch(fromToken, toToken, tpAmount, slippage, address);

    //Calculer cette unit amount pourquoi alors qu'ils ont les infos ? fin bon c'est dans la doc :
    //https://docs.instadapp.io/faq/connectors/calculate-unitamt
    //Pas facile à trouver !
    let unitAmt = caculateUnitAmt(data1inch.toTokenAmount, data1inch.fromTokenAmount , data1inch.toToken.decimals, data1inch.fromToken.decimals, slippage);
    const n = web3.utils.randomHex(32);
    spells.add({
      connector: "1INCH-V4-A",
      method: "sell",
      args: [
          toToken,   //ADDR BUY
          fromToken, //ADDR SELL
          BigNumber(data1inch.fromTokenAmount),   //AMOUNT SELL
          unitAmt,
          data1inch.tx.data,
          n
      ]
    });

    //Ceci corrige le low-level call failed ! effectivement le slippage n'était pas pris en compte !
    // let deposit = (data1inch.toTokenAmount - (data1inch.toTokenAmount * (slippage/100))).toFixed(0);
    //Optionnel avec le n
    spells.add({
      connector: 'aave_v2',
      method: 'payback',
      args: [
          toToken,
          BigNumber("115792089237316195423570985008687907853269984665640564039457584007913129639935"),
          2,
          n,
          0
      ],
    });

    spells.add({
      connector: 'aave_v2',
      method: 'withdraw',
      args: [
          fromToken,
          BigNumber(tpAmountFee),
          0,
          0
      ],
    });

    spells.add({
        connector: 'INSTAPOOL-C',
        method: 'flashPayback',
        args: [
            fromToken,
            BigNumber(tpAmountFee),
            0,
            0
        ],
    });

    let cd = dsa.instapool_v2.encodeFlashCastData(spells);
    let flashloanWrappedSpells = dsa.Spell();

    flashloanWrappedSpells.add({
        connector: 'INSTAPOOL-C',
        method: 'flashBorrowAndCast',
        args: [fromToken, BigNumber(tpAmount), "5", cd, "0x"],
    });

    return flashloanWrappedSpells;
}


async function balanceOf(keyName, walletAddress) {
    const minABI = [
      // balanceOf
      {
        constant: true,
        inputs: [{ name: "_owner", type: "address" }],
        name: "balanceOf",
        outputs: [{ name: "balance", type: "uint256" }],
        type: "function",
      },
    ];

    ///const tokenAddress = token.getTokenByKey("adai").address;
    const tokenAddress = token.getTokenByKey(keyName).address;
    // const walletAddress = "0x243AbC345ef9bF6E822fb1b9FE937fd9C0921848";
    const contract = new web3.eth.Contract(minABI, tokenAddress);

    return await contract.methods.balanceOf(walletAddress).call(); // 29803630997051883414242659
    // return web3.utils.fromWei(result); // 29803630.997051883414242659
}

async function getAtokenByVariableDebtToken(keyName) {
    let mappingDebtCol = {
      "variableDebtmWETH": "aeth",
      "variableDebtmWBTC": "awbtc",
      "variableDebtmDAI" : "adai",
      "variableDebtmWMATIC": "awmatic",
    };

    let t = token.getTokenByKey(keyName);
    if(t.hasOwnProperty('root')) {
        return t;
    } else {
        return token.getTokenByKey(mappingDebtCol[keyName]);
    }
}

async function balanceInDai(keyName, fromToken, listPricesDai, kRoot) {
    let balance = await balanceOf(keyName, ACCOUNT_ADDR);
    let balanceInDAI = 0;
    let tokenPriceAmount = 0;
    if(balance > 0) {
        if(fromToken === DAI_TOKEN) {
            balanceInDAI = web3.utils.fromWei(balance, 'ether');
            return {"inTokenToWei": balance, "inDaiToWei" : balance, "inDai" : balanceInDAI};
        }

        let tRoot = token.getTokenByRoot(kRoot);
        let unitName = getKeyByValue(web3.utils.unitMap, Math.pow(10, tRoot.decimals).toString());
        balanceInDAI = round((web3.utils.fromWei(balance, unitName) * listPricesDai[kRoot]), tRoot.decimals);
        tokenPriceAmount = BigNumber(Math.round(toWei(balanceInDAI, kRoot))).toString();
    }

    return {"inTokenToWei": balance, "inDaiToWei" : tokenPriceAmount, "inDai" : balanceInDAI};
}

function getPriceDaiWeiToTokenWei(listPricesInDai, amountInTokenWei, rootToken) {
    let amountInToken = (web3.utils.fromWei(amountInTokenWei.toString()) / listPricesInDai[rootToken.root]).toFixed(rootToken.decimals);
    return BigNumber(Math.round(toWei(amountInToken, rootToken.root)).toString());
    // return BigNumber(Math.round(amountInToken * Math.pow(10, rootToken.decimals)).toString());
}


async function vaultInfoPrices(listPricesInDai) {
    const prices = {
        'variableDebtmWETH' : ETH_TOKEN,
        'variableDebtmWBTC' : BTC_TOKEN,
        'variableDebtmDAI'  : DAI_TOKEN,
        'variableDebtmWMATIC' : MATIC_TOKEN,
        'awmatic'  : MATIC_TOKEN,
        'awbtc'  : BTC_TOKEN,
        'aeth' : ETH_TOKEN,
        'adai' : DAI_TOKEN,
    };

    const debtList = [
        "variableDebtmWETH",
        "variableDebtmWBTC",
        "variableDebtmDAI",
        "variableDebtmWMATIC"
    ];

    const colList = [
        "awmatic",
        "awbtc",
        "aeth",
        "adai",
    ];

    for (const key in prices) {
      let kRoot = (await getAtokenByVariableDebtToken(key)).root;
      let amountInfo = await balanceInDai(`${key}`, `${prices[key]}`, listPricesInDai, kRoot);
      prices[key] = amountInfo;
    }

    //Récupération des noms de debt et de col.
    //dt = debt_token; ct = col_token
    let dtKeyDebt = token.getDebtTokenByDebtRoot(DEBT_TOKEN).key;
    let dtKeyCol  = token.getTokenByRoot(DEBT_TOKEN).key;
    let ctKeyDebt = token.getDebtTokenByDebtRoot(COL_TOKEN).key;
    let ctKeyCol  = token.getTokenByRoot(COL_TOKEN).key;

    //Si le col (debt token) est plus grand que le debt (debt token) alors il faut swap
    // Ex : 5 Dai en col et 0 Dai en Debt alors que la col normalement est le eth
    // Dans l'exemple il faut donc swap le dai en eth (paramètre est l'eth)
    //On va pas switch pour 2 putain de dai !
    let swapCol  = (prices[dtKeyCol].inDai > prices[dtKeyDebt].inDai) && prices[dtKeyCol].inDai > 2;
    //Ici c'est l'inverse !
    let swapDebt = prices[ctKeyDebt].inDai > prices[ctKeyCol].inDai && prices[ctKeyDebt].inDai > 2;


    let totalCol = 0;
    for (const value of colList) {
        totalCol = BigNumber(prices[value].inDaiToWei) * 1 + BigNumber(totalCol) * 1;
    }

    let totalDebt = 0;
    for (const value of debtList) {
        totalDebt = BigNumber(prices[value].inDaiToWei) * 1 + BigNumber(totalDebt) * 1;
    }

    let ratio = BigNumber(totalDebt) / BigNumber(totalCol) ;

    let totalColInDai  = web3.utils.fromWei(BigNumber(totalCol).toString()) * 1;
    let totalDebtInDai = web3.utils.fromWei(BigNumber(totalDebt).toString()) * 1;

    return {
        'ratio' : ratio, 'prices' : prices,
        'totalCollateral' : totalCol, 'totalDebt' : totalDebt,
        'totalCollateralInDai' : totalColInDai, 'totalDebtInDai': totalDebtInDai,
        'swap': {
            'debt': swapDebt, 'colKey': dtKeyCol,
            'col': swapCol,   'debtKey': ctKeyDebt}
    }
}

async function getData1Inch(fromToken, toToken, amount, slippage, address) {
    if(PARASWAP === true) {
        return urlparaswap(fromToken, toToken, amount, slippage, address);
    }

    let urlApi1inch = url1inch(fromToken, toToken, amount, slippage, address);
    return apiCaller(urlApi1inch);
}

function checkConfig() {
    if(RATIO_MAX <= RATIO_TARGET) {
        throw new Error('Le ratio max ('+RATIO_MAX+') doit être plus grand que la target ('+RATIO_TARGET+').');
    }
    if(RATIO_MIN >= RATIO_TARGET) {
        throw new Error('Le ratio min ('+RATIO_MAX+') doit être plus petit que la target ('+RATIO_TARGET+').');
    }

    if(RATIO_MIN > 0.74 && RATIO_TARGET > 0.74 && RATIO_MAX > 0.74) {
        throw new Error(`Les ratios doivent être plus petit que 0.74 car si non le vault est liquidé
        ${RATIO_MIN}, ${RATIO_TARGET}, ${RATIO_MAX}`);
    }
}

async function getNonceTransaction() {
    let notPendingNonce = await web3.eth.getTransactionCount(ETH_ADDR);
    let nonce = await web3.eth.getTransactionCount(ETH_ADDR, 'pending');
    nonce = nonce;
    return {'nonce' : nonce, 'pendingNonce': notPendingNonce, 'pending': (notPendingNonce !== nonce)};
}

async function swapColAndDebt(vault, ct, dt, listPricesInDai) {
    if(vault.swap.debt || vault.swap.col) {
        //Effectuer le swaping !
        let spells = dsa.Spell();
        let account = await dsa.setInstance(DSA_ID_ACCOUNT);

        if(vault.swap.col) {
            console.log("Un swap de col en "+ct.toUpperCase()+" vers "+COL_TOKEN.toUpperCase()+ " sera effectué !");
            let priceColSwap = vault.prices[vault.swap.colKey];
            //JE SUIS VRAIMENT UN BOULET --> faut diviser par le "token" donc des "dai / 1" ou "eth / prix en ETH".
            let calCAMount = round(priceColSwap.inDai / listPricesInDai[ct], token.getTokenByRoot(ct).decimals - 1);
            let cAmount = toWei(calCAMount, ct);

            spells = await colSwap(
                spells,
                token.getTokenByRoot(ct).rootAddress,
                token.getTokenByRoot(COL_TOKEN).rootAddress,
                cAmount,
                SLIPPAGE,
                account.address,
            );
            return await createCast(spells, DRYRUN);
        }

        if(vault.swap.debt) {
            console.log("Un swap de debt en "+dt.toUpperCase()+" vers "+DEBT_TOKEN.toUpperCase()+ " sera effectué !");
            let priceDebtSwap =  vault.prices[vault.swap.debtKey];

            //En faire une fonction pour le toWei
            let calTAMount = round((priceDebtSwap.inDai / listPricesInDai[DEBT_TOKEN]), token.getTokenByRoot(DEBT_TOKEN).decimals - 1);
            let tAmount = BigNumber(Math.round(toWei(calTAMount, DEBT_TOKEN)));

            spells = await debtSwap(
                spells,
                token.getTokenByRoot(DEBT_TOKEN).rootAddress, //DAI
                token.getTokenByRoot(dt).rootAddress,         //ETH
                tAmount,                                      //DAI
                SLIPPAGE,
                account.address,
            );
            return await createCast(spells, DRYRUN);
        }
        return false;
    }
    return false;
}

function round(num, max) {
    let re = new RegExp('^-?\\d+(?:\.\\d{0,' + (max || -1) + '})?');
    return num.toString().match(re)[0];
}

function toWei(amount, tokenName) {
    let unitName = getKeyByValue(web3.utils.unitMap, Math.pow(10, token.getTokenByRoot(tokenName).decimals).toString());
    return web3.utils.toWei(amount.toString(), unitName);
}

async function App(stopLeverage = false) {
    let ai = {};
    ai.stopLeverage = stopLeverage;

    console.log("-------Auto Insta---------");
    console.log(new Date().toISOString());
    console.log("Target: " + RATIO_TARGET * 100 + "% - Max: " + RATIO_MAX * 100 + "% - Min: " + RATIO_MIN * 100 + "%");
    console.log("Stop  : L " + STOP_LOST + " - W : " + STOP_WIN);
    console.log("--------------------------");
    checkConfig(); //Check config .env was ok.

    let listPricesInDai = await apiGetPrices();
    let vault = await vaultInfoPrices(listPricesInDai);
    let netValue = (vault.totalCollateralInDai - vault.totalDebtInDai);
    let betTokenName = (COL_TOKEN === 'dai') ? DEBT_TOKEN : COL_TOKEN;

    //Save the last prices and net value
    ai.listPricesInDai = listPricesInDai;
    ai.netValue = netValue;


    console.log("1 " + betTokenName.toUpperCase()+ " = "+ listPricesInDai[betTokenName] + " DAI");
    console.log("--------------------------");
    if((STOP_WIN > 0 && listPricesInDai[betTokenName] >= STOP_WIN)
        || (STOP_LOST > 0 && listPricesInDai[betTokenName] <= STOP_LOST)
        || stopLeverage === true
    ) {
        console.log("/!\\ STOP LEVERAGE [RATIO 0%] /!\\");
        RATIO_TARGET = 0;
        RATIO_MIN = -1;
        RATIO_MAX = 0.00000001;
        ai.stopLeverage = true;
    }

    let ct = (vault.swap.col) ? DEBT_TOKEN : COL_TOKEN;
    let dt = (vault.swap.debt) ? COL_TOKEN : DEBT_TOKEN;
    let mFixed = 7;

    console.log("Net Value  : " + netValue.toFixed(mFixed) + " Dai");
    let netValueInCol = (netValue / listPricesInDai[COL_TOKEN]).toFixed(mFixed) + " " + COL_TOKEN.toUpperCase();
    if((COL_TOKEN === 'dai')) {
        netValueInCol =  netValue / (listPricesInDai[DEBT_TOKEN]).toFixed(mFixed) + " " + DEBT_TOKEN.toUpperCase();
    }
    console.log("             " + netValueInCol);
    console.log("--------------------------");
    console.log("Ratio      : " + vault.ratio *100 + " %");
    console.log("Col  ("+ct +") : " + vault.totalCollateralInDai.toFixed(mFixed) + " DAI");
    console.log("Debt ("+dt+") : " + vault.totalDebtInDai.toFixed(mFixed) + " DAI");
    console.log('--------------------------');


    //Set nonce for the next transaction. and validate the transaction still waiting
    let nt = await getNonceTransaction();
    nonce = nt.nonce;
    ai.transaction = nt;
    if (nt.pending) {
        console.error('Waiting Nonce : '+ nt.pendingNonce);
        console.error('Nonce : ' + nt.nonce);
        console.error('Transaction is waiting...');

        if(forceNonce > 0) {
            console.log('The transaction will be forced');
            nonce = forceNonce;
            forceNonce = 0;
        } else {
            return ai;
        }
    }

    //Switch col and debt in the same time/transaction
    let swap = await swapColAndDebt(vault, ct, dt, listPricesInDai);
    if(swap !== false) {
        ai.transaction.tx = swap;
        return ai;
    }


    //@todo doublons changer cette merde !
    let mappingDebtCol = {
      "variableDebtmWETH": "aeth",
      "variableDebtmWBTC": "awbtc",
      "variableDebtmDAI" : "adai",
      "variableDebtmWMATIC": "awmatic",
    };
    let mappingColDebt = {};

    let toClean = false;
    let clean = [];

    for (const debtName in mappingDebtCol) {
        let colName = mappingDebtCol[debtName];
        mappingColDebt[colName] = debtName;

        let colPrice  = vault.prices[colName].inDai;
        let debtPrice = vault.prices[debtName].inDai;
        let realDebt = colPrice - debtPrice;

        //Start Nettoyage Algo.
        if(debtPrice >= 2 && realDebt > 0) {
            console.log('Debt : ' + debtName);
            console.log('Le collatérale est le même que la debt.');
            console.log('Un nettoyage sera effectué.');
            clean[debtName] = vault.prices[debtName];
            toClean = true;
        }
    }

    if(toClean) {
        console.log('Cleaning...');
        ai.transaction.tx = await cleanUselessDebt(clean, DRYRUN);
        return ai;
    }

    let colToken = token.getTokenByRoot(COL_TOKEN);
    let debtRootToken = token.getTokenByRoot(DEBT_TOKEN);
    let debtToken = token.getTokenByKey(mappingColDebt[debtRootToken.key]);

    // if(!(vault.prices[debtToken.key].inDai > 0 && vault.prices[colToken.key].inDai)) {
    //     console.log("Il faut switch les debts et collateral, cela n'est pas encore fait.");
    //     //@TODO SI LE COLLATERAL N'EST PAS LE CHOISI DANS LA CONFIG
    //     //@TODO IL FAUT SWAP !
    //     return;
    // }

    let colVal = vault.prices[colToken.key];
    let debtVal = vault.prices[debtToken.key];

    //Calcule du rééquilibrage
    let newDebt = ((colVal.inDai - debtVal.inDai) * RATIO_TARGET) / (1 - RATIO_TARGET);
    let newCol  = (newDebt / RATIO_TARGET);

    let showNewRatio = function () {
        console.log("New Ratio  : " + (newDebt / newCol) *100 + " %");
        console.log("New Col    : " + newCol + " Dai");
        console.log("New Debt   : " + newDebt + " Dai");
        console.log('--------------------------');
    };

    //Leverage ou withdraw alogrithme
    if((colToken.hasOwnProperty('rootAddress')
        && vault.totalCollateral > Math.pow(10, 18)
        && vault.ratio <= RATIO_MIN)
    ) {
        //Calculer combien il faut pour équilibrer
        let levDebt = newDebt - debtVal.inDai;
        showNewRatio();
        console.log("Withdraw   : "    + levDebt + " Dai");

        if(levDebt <= 0) {
            throw new Error("Withdraw négatif ! Ne devrait jamais arrivé");
        }

        //Max Empruntable :
        let cMp = (vault.totalDebtInDai + levDebt) / vault.totalCollateralInDai;
        let maxPayable =  (token.getTokenByRoot(COL_TOKEN).factor - cMp) * vault.totalCollateralInDai;
        console.log('Borrowable : ' + maxPayable + " Dai");

        let bld= ((levDebt * listPricesInDai.matic) + levDebt) * 2;
        let borrowWMatic = (Math.abs(levDebt / maxPayable)) >= 0.20 ? bld : -1;
        console.log("Borrow with AVEE Matic : " + borrowWMatic + " Dai");

        let levDebtInDaiWei = BigNumber(Math.round(levDebt * Math.pow(10, token.getTokenByRoot("dai").decimals)));
        let borrowWMaticInWei = -1;

        //Emprunter si le somme pour équilibré est trop risqué (état de liquidation).
        //Certainement pas, nécessaire pour un effet de levier, mais on ne sait jamais
        if(vault.ratio >= (token.getTokenByRoot(COL_TOKEN).factor - 0.1)) {
            borrowWMaticInWei = BigNumber(Math.round(bld * Math.pow(10, token.getTokenByKey('awmatic').decimals)));
        } else if (borrowWMatic > 0) {
            borrowWMaticInWei = BigNumber(Math.round(borrowWMatic * Math.pow(10, token.getTokenByKey('awmatic').decimals)));
        }


        let amount = levDebtInDaiWei;
        if(DAI_TOKEN !== debtRootToken.rootAddress) {
            //GetPrice from 1Inch
            amount =  getPriceDaiWeiToTokenWei(listPricesInDai, levDebtInDaiWei, debtRootToken);
            // amount = (await getData1Inch(DAI_TOKEN, debtRootToken.rootAddress, levDebtInDaiWei, 0, ACCOUNT_ADDR, 1)).toTokenAmount;
        }

        //Cast new debt
        ai.transaction.tx = await castLeverage(colToken.rootAddress, debtRootToken.rootAddress,
            amount, SLIPPAGE, borrowWMaticInWei, DRYRUN);
    }
    //Save ou payback algorithme
    else if(
        (colToken.hasOwnProperty('rootAddress')
        && vault.totalCollateral > Math.pow(10, 18)
        && vault.ratio >= RATIO_MAX)
    ){
        let saveDebt = debtVal.inDai - newDebt; // debt actuelle moins la debt "cible". Ce qui donne ce qu'on veut sauver.
        showNewRatio();
        console.log("Payback    : "     + saveDebt);

        if(saveDebt <= 0) {
            throw new Error("Withdraw négatif ! Ne devrait jamais arrivé");
        }

        //Max Empruntable :
        let cMp = vault.totalDebtInDai / (vault.totalCollateralInDai - saveDebt);
        let maxPayable =  (token.getTokenByRoot(COL_TOKEN).factor - cMp) * vault.totalCollateralInDai;
        console.log('Borrowable : ' + maxPayable);

        //+ 20% go emprunter ! et emprumter un max !
        let bsd = ((saveDebt * listPricesInDai.matic) + saveDebt) * 2;
        let borrowWMatic = (Math.abs(saveDebt / maxPayable)) >= 0.20 ? bsd : -1;
        console.log("Borrow with AVEE Matic : " + borrowWMatic);

        let saveDebtInDaiWei = BigNumber(saveDebt * Math.pow(10, token.getTokenByRoot("dai").decimals));
        let borrowWMaticInWei = -1;

        //Si on est à  la limite de la liquidation on s'en fou on emprumpte basta !
        if(vault.ratio >= (token.getTokenByRoot(COL_TOKEN).factor - 0.1)) {
            borrowWMaticInWei = BigNumber(Math.round(bsd * Math.pow(10, token.getTokenByKey('awmatic').decimals)));
        } else if (borrowWMatic > 0) {
            borrowWMaticInWei = BigNumber(Math.round(borrowWMatic * Math.pow(10, token.getTokenByKey('awmatic').decimals)));
        }


        let amount = saveDebtInDaiWei;
        if(DAI_TOKEN !== colToken.rootAddress) {
            //GetPrice from 1Inch
            // let unitName = getKeyByValue(web3.utils.unitMap, Math.pow(10, colToken.decimals).toString());
            // let amountInDAI = web3.utils.fromWei(saveDebtInDaiWei, unitName) / listPricesInDai[colToken.root];
            // amount = web3.utils.toWei(amountInDAI.toString(), unitName);
            amount =  getPriceDaiWeiToTokenWei(listPricesInDai, saveDebtInDaiWei, colToken);
            // amount = (await getData1Inch(DAI_TOKEN, colToken.rootAddress, saveDebtInDaiWei, 0, ACCOUNT_ADDR, 1)).toTokenAmount;
        }

        ai.transaction.tx = await castSave(colToken.rootAddress, debtRootToken.rootAddress,
            amount, SLIPPAGE, borrowWMaticInWei, DRYRUN);
    }

    return ai;
}


function waitingTransaction(lastTx){
    console.log('--------------------');
    console.log('See tx : https://polygonscan.com/tx/' + lastTx);
    process.stdout.write('Transaction pending.');
    let sip = setInterval(async () => {
        process.stdout.write(".");
    }, 500);
    let sit = setInterval(async () => {
        try {
            let waitTx = await web3.eth.getTransactionReceipt(lastTx);
            if(waitTx !== null) {
                console.log('.successfully completed!');
                console.log(new Date().toISOString());
                console.log('--------------------');
                console.log("");
                console.log("");
                clearInterval(sit);
                clearInterval(sip);
            }
        } catch (e) {
            clearInterval(sit);
            clearInterval(sip);
            console.error(e);
        }
    }, 2000);
    return [sip, sit];
}

function determinatePrices(lastPrices, currentPrices, netValue) {
    let col = COL_TOKEN;
    let debt = DEBT_TOKEN;
    let cDate = new Date().getTime();

    if(Object.keys(lastPrices).length <= 0) {
        let d = new Date();
        d.setDate(d.getDate() + 1);
        d.setHours(2,5,0,0);

        lastPrices[col] = currentPrices[col];
        lastPrices[debt] = currentPrices[debt];
        lastPrices["resetDate"] = d.getTime();
        lastPrices.netValue = netValue;
        return lastPrices;
    } else {
        if(lastPrices.resetDate <= cDate) {
            return determinatePrices({}, currentPrices);
        }
    }
    return {};
}

function Run() {
    let ctp = 1; //count cycle transaction pending
    let stopLeverage = false;
    let lastTx = 0;
    let ai = {}; let sip; let sit;

    let im = async () => {
        clearInterval(sit);
        clearInterval(sip);

        try {
            if(typeof ai.transaction !== 'undefined'
                && typeof ai.transaction.pending !== 'undefined'
                && ai.transaction.pending
            ) {
               ctp++;
               if(ctp >= MAX_TRANSACTION_PENDING) {
                   ctp = 0;
                   //force the nonce !
                   forceNonce = ai.transaction.pendingNonce;
               }
            }
            ai = await App(stopLeverage);
            stopLeverage = ai.stopLeverage;

            if (typeof ai.transaction !== 'undefined'
                && typeof ai.transaction.tx !== 'undefined'
                && ai.transaction.tx.length > 1
            ) {
                lastTx = ai.transaction.tx;
                [sip, sit] = waitingTransaction(lastTx);
            }
            // console.log(ai);
        } catch (e) {
            console.error(e);
        }
    };
    im(); //launch at start !
    let si = setInterval(im, TICKING);
}

Run();

